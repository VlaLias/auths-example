package controllers;

import models.Login;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import play.mvc.With;
import security.Secured;
import security.SecuredAction;
import views.html.loginPage;
import views.html.secretPage;

import javax.inject.Inject;

public class SomeController extends Controller {

    @Inject
    private FormFactory formFactory;

    public Result displaySimplePage() {
        return ok("This is a page for all users");
    }

    @With(SecuredAction.class)
    public Result displaySecretPage2() {
        return ok("Some secret information must be here.");
    }

    @Security.Authenticated(Secured.class)
    public Result displaySecretPage() {
        return ok(secretPage.render());
    }

    public Result login() {
        return ok(loginPage.render(formFactory.form(Login.class)));
    }

    public Result logout() {
        session().clear();
        return redirect(routes.SomeController.login());
    }

    public Result authenticate() {
        Form<Login> loginForm = formFactory.form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return redirect(routes.SomeController.login());
        } else {
            session().clear();
            session("login", loginForm.get().getLogin());
            return redirect(routes.SomeController.displaySecretPage());
        }
    }

}
