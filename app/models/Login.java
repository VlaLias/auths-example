package models;

import util.SimpleLoginValidator;

public class Login {

    private String login;

    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String validate() {
        return SimpleLoginValidator.isValidLogin(login, password) ? null : "Invalid user or password";
    }
}
